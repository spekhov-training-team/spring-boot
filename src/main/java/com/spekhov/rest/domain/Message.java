package com.spekhov.rest.domain;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "MESSAGE")
@Builder(toBuilder = true)
@Data
public class Message extends AbstractEntity {

    private String text;
    private String tag;

    public Message() {

    }

    public Message(String text, String tag) {
        this.text = text;
        this.tag = tag;
    }
}
