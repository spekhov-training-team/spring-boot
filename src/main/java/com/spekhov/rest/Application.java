package com.spekhov.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@SpringBootApplication
public class Application {

    static Logger logger = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
//        SpringApplication application = new SpringApplication();
//        application.setBannerMode(Banner.Mode.CONSOLE);
//        application.setDefaultProperties(getProperties("default.properties"));
//        ConfigurableApplicationContext context = application.run(Application.class, args);
    }

    static private Properties getProperties(String path) {
        Properties properties = new Properties();
        try (InputStream inputStream = new FileInputStream(path)) {
            properties.load(inputStream);
        } catch (FileNotFoundException e) {
            logger.error("File {} not found", path);
        } catch (IOException e) {
            logger.error("Read error.", e);
        }
        return properties;
    }
}
