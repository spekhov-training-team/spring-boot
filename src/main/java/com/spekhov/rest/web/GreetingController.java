package com.spekhov.rest.web;

import com.spekhov.rest.service.MainService;
import com.spekhov.rest.domain.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

@Controller
public class GreetingController {

    @Autowired
    private MainService mainService;

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name = "name", required = false, defaultValue = "world") String name,
                           Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping("/simple/json")
    @ResponseBody
    public Map<String, Object> json() {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "Alex");
        map.put("age", 24);
        return map;
    }

    @GetMapping
    public String main(Model model) {
        Iterable<Message> messages = mainService.getMessages();
        model.addAttribute("messages", messages);
        return "main";
    }

    @PostMapping
    @RequestMapping(params = "add")
    public String add(@RequestParam String text, @RequestParam String tag, Model model) {
        Iterable<Message> messages = mainService.addMessage(text, tag);
        model.addAttribute("messages", messages);
        return "main";
    }

    @PostMapping()
    @RequestMapping(params = "delete")
    public String deleteMessages(Model model) {
        mainService.cleanMessage();
        return "main";
    }
}
