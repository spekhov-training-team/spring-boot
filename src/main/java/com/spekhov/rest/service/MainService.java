package com.spekhov.rest.service;

import com.spekhov.rest.domain.Message;
import com.spekhov.rest.repository.MessageRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

@Service
@Slf4j
public class MainService {

    @Autowired
    private MessageRepository messageRepository;

    public Iterable<Message> getMessages() {
        return messageRepository.findAll();
    }

    public Iterable<Message> addMessage(String text, String tag) {
        if (StringUtils.isEmpty(text) || StringUtils.isEmpty(text)) {
            log.error("Text and tag not must be empty!");
            return null;
        }
        try {
            messageRepository.save(new Message(text, tag));
            return messageRepository.findAll();
        } catch (Exception ex) {
            log.error("Saving message failed");
            return null;
        }
    }

    public void cleanMessage() {
        messageRepository.deleteAll();
    }
}
